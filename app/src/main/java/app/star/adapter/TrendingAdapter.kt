package app.star.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.star.R
import app.star.databinding.SearchStarAdapterBinding
import app.star.databinding.TrendingAdapterBinding

class TrendingAdapter(arrayList: ArrayList<String>, val listener:TrendingAdapterListener) :
    RecyclerView.Adapter<TrendingAdapter.ViewHolder>() {
    private var arrayList: ArrayList<String>


    init {
        setHasStableIds(true)
        this.arrayList = arrayList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: TrendingAdapterBinding = DataBindingUtil.inflate(layoutInflater, R.layout.trending_adapter, parent, false)
        return ViewHolder(binding)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(arrayList[position])
        holder.binding.mainLayout.setOnClickListener {
            listener.onItemClickListener(arrayList[position])
        }
    }

    fun notifyAdapter(resultList: ArrayList<String>) {
        arrayList = resultList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }


    class ViewHolder(val binding: TrendingAdapterBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: String) {
        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface TrendingAdapterListener{
        fun onItemClickListener(clickedDat: String)
    }
}