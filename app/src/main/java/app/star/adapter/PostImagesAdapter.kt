package app.star.adapter

import android.net.Uri
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.star.R
import app.star.binding.setSafeOnClickListener
import app.star.databinding.PostImageAdapterBinding

class PostImagesAdapter (arrayList: ArrayList<String>, val listener:ImageAdapterListener) :
    RecyclerView.Adapter<PostImagesAdapter.ViewHolder>() {
    private var arrayList: ArrayList<String>


    init {
        setHasStableIds(true)
        this.arrayList = arrayList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: PostImageAdapterBinding = DataBindingUtil.inflate(layoutInflater, R.layout.post_image_adapter, parent, false)
        return ViewHolder(binding)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(arrayList[position])

        holder.binding.selectedImage.setSafeOnClickListener {
            if (arrayList[position]=="Add More"){
                listener.addImage()
            }
        }
    }

    fun notifyAdapter(resultList: ArrayList<String>) {
        arrayList = resultList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }


    class ViewHolder(val binding: PostImageAdapterBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: String) {

            if (item=="Add More"){
                binding.selectedImage.setPadding(70,70,70,70)
                binding.selectedImage.setImageResource(R.drawable.ic_add_black)
                binding.selectedImage.setBackgroundResource(R.drawable.image_white_rounded)
                binding.txtAddImage.visibility= View.VISIBLE

            }
            else{
                binding.txtAddImage.visibility= View.GONE
                binding.selectedImage.setPadding(0,0,0,0)
                val bitmap =
                    MediaStore.Images.Media.getBitmap(binding.selectedImage.context.getContentResolver(), Uri.parse(item))
                binding.selectedImage.setImageBitmap(bitmap)

            }

        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface ImageAdapterListener{
        fun onItemClickListener(itemData: String)
        fun addImage()
    }
}