package app.star.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.star.R
import app.star.databinding.CategoryAdapterBinding


class CategoryAdapter(arrayList: ArrayList<String>, val listener:CategoryAdapterListener) :
    RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    private var arrayList: ArrayList<String>


    init {
        setHasStableIds(true)
        this.arrayList = arrayList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: CategoryAdapterBinding = DataBindingUtil.inflate(layoutInflater, R.layout.category_adapter, parent, false)
        return ViewHolder(binding)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(arrayList[position])
        holder.binding.categoryCard.setOnClickListener {
            listener.onItemClickListener(arrayList[position])
        }



    }

    fun notifyAdapter(resultList: ArrayList<String>) {
        arrayList = resultList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }


    class ViewHolder(val binding: CategoryAdapterBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: String) {
        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface CategoryAdapterListener{
        fun onItemClickListener(clickedDat: String)
    }
}

