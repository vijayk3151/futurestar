package app.star.module

import app.star.activity.login.LoginViewModel
import app.star.activity.post.PostActivity
import app.star.activity.post.PostViewModel
import app.star.activity.register.RegisterViewModel
import org.koin.dsl.module

val viewModelModule = module {

    single {
        RegisterViewModel(get(), get())
    }
    single {

        PostViewModel(get(), get())
    }
    single { LoginViewModel(get(),get()) }
}
