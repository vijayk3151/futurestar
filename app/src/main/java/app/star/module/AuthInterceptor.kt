package app.star.module

import app.star.util.AppConstant
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response


class AuthInterceptor () : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
        builder.header("accept", "application/json")
        builder.header("token",AppConstant.authToken)
         return chain.proceed(builder.build())

       /* var mainResponse = chain.proceed(buildRequest(chain))
        return mainResponse*/
    }


    private fun buildRequest(chain: Interceptor.Chain): Request {

        val original = chain.request()
        original.newBuilder()
            .method(original.method(), original.body())
            .header("Accept","application/json")
            .header("dtoken", "dtoken")
            .build()

        return original
    }


}