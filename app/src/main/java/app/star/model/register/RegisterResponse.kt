package app.star.model.register

data class RegisterResponse(
    val email: String,
    val id: Int,
    val role: String,
    val systemName: String
)