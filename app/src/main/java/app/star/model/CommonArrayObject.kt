package app.star.model

data class CommonArrayObject<T>(
    val `data`: List<T>,
    val message: String,
    val success: Boolean
)