package app.star.model

data class CommonObject<T>(
    val `data`: T,
    val message: String,
    val status: Boolean,
    val status_code: Int
)