package app.star.util

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.WindowManager
import app.star.R


object CallProgressWheel {

    private var progressDialog: ProgressDialog? = null

    val isDialogShowing: Boolean
        get() {
            try {
                return progressDialog != null && progressDialog!!.isShowing
            } catch (e: Exception) {
                return false
            }

        }

    /*
      Displays custom loading dialog
     */
    fun showLoadingDialog(context: Context) {
        try {
            if (isDialogShowing) {
                dismissLoadingDialog()
            }

            if (context is Activity) {
                if (context.isFinishing) {
                    return
                }
            }
            progressDialog = ProgressDialog(context)
            progressDialog!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            progressDialog!!.show()
            val layoutParams = progressDialog!!.window!!.attributes
            layoutParams.dimAmount = 0.5f
            progressDialog!!.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
            progressDialog!!.setCancelable(false)
            progressDialog!!.setContentView(R.layout.layout_progress)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun dismissLoadingDialog() {
        try {
            if (progressDialog != null) {
                progressDialog!!.dismiss()
                progressDialog = null
            }
        } catch (e: Exception) {
            Log.e("e", "=" + e)
        }

    }
}
