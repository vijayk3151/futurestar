package app.star.util

object SharedKey {

    const val first_name = "first_name"
    const val last_name="last_name"
    const val email="email"
    const val user_name="user_name"
    const val password="password"
    const val password_confirmation="password_confirmation"
    const val role="role"
}