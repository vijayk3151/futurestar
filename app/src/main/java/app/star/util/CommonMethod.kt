package app.star.util

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.text.TextUtils
import android.util.Patterns
import androidx.appcompat.app.AlertDialog


object CommonMethod {

    fun isEmailValid(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun notEmptyFieldValidation(value: String):Boolean{
        return value.isNotEmpty()
    }

    fun showAlertDialog(context: Context, message: String, title:String?="Alert!") {
        val dialogBuilder = AlertDialog.Builder(context)

        // set message of alert dialog
        dialogBuilder.setMessage(message)
            // if the dialog is cancelable
            .setCancelable(false)
            // positive button text and action
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
            })

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle(title)
        // show alert dialog
        if (!(context as Activity).isFinishing()) {
            alert.show()
        }

    }


    fun showSuccessDialog(context: Context, message: String, dialogCallBack: DialogCallBack) {
        val dialogBuilder = AlertDialog.Builder(context)

        // set message of alert dialog
        dialogBuilder.setMessage(message)
            // if the dialog is cancelable
            .setCancelable(false)
            // positive button text and action
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                dialogCallBack.positiveClick()
                dialog.cancel()
            })

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle("Alert!")
        // show alert dialog
        if (!(context as Activity).isFinishing()) {
            alert.show()
        }

    }
}
interface DialogCallBack{
    fun positiveClick()
}