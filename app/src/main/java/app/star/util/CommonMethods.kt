package app.star.util

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.ContextWrapper
import android.content.DialogInterface
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import java.io.*
import java.lang.IndexOutOfBoundsException
import java.net.URLConnection
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CommonMethods {


    companion object {
        var PLACE_AUTOCOMPLETE_REQUEST_CODE = 1
        var info: ActivityInfo? = null
        var date: Date? = null
        var d: String? = null
        var idNumberForServer = ""
        var keyAlredyCheckin: Boolean = false


        fun showSnackbarMessage(context: Context, message: String, color: Int) {
            val snackbar = Snackbar.make(
                (context as Activity).findViewById(android.R.id.content),
                message,
                Snackbar.LENGTH_LONG
            )
            snackbar.view.setBackgroundColor(ContextCompat.getColor(context, color))
            snackbar.show()
        }

        /**
         * Default date format
         */

        fun editTextSetValue(editText: EditText) {
            editText.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {}

                override fun beforeTextChanged(
                    s: CharSequence, start: Int,
                    count: Int, after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence, start: Int,
                    before: Int, count: Int
                ) {

                    if (start == 0 && before == 0) {
                        //Get the input
                        val input = s.toString().toUpperCase();
                        //Capitalize the input (you can also use StringUtils here)
                        val output = input.toUpperCase()
                        //Set the capitalized input as the editText text
                        editText.setText(output);
                        //Set the cursor at the end of the first character
                        editText.setSelection(1);
                    }
                }
            })


        }

        fun showToastMessage(context: Context, message: String) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }

        fun CreateLog(message: String) {
            Log.i("VMS_NEW", message)
        }

        fun CreateLogForScan(message: String) {
            Log.i("==SCAN==", message)
        }

        fun CreateLogNotification(message: String) {
            Log.i("==Notification==", message)
        }

        fun setImage(
            applicationContext: Context,
            imageView: ImageView,
            mOutputPath: String?,
            placeholder: Int
        ) {
            Glide.with(applicationContext)
                .load(mOutputPath)
                .apply(RequestOptions().placeholder(placeholder).error(placeholder))
                .into(imageView)

        }

        fun showAlertDialog(context: Context, message: String) {
            val dialogBuilder = AlertDialog.Builder(context)

            // set message of alert dialog
            dialogBuilder.setMessage(message)
                // if the dialog is cancelable
                .setCancelable(false)
                // positive button text and action
                .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                })

            // create dialog box
            val alert = dialogBuilder.create()
            // set title for alert dialog box
            alert.setTitle("Alert!")
            // show alert dialog

            alert.show()

        }

        fun showAlertWithCallBack(
            context: Context, title: String,
            message: String, dialogCallBack: DialogCallBack
        ) {
            val dialogBuilder = AlertDialog.Builder(context)

            // set message of alert dialog
            dialogBuilder.setMessage(message)
                // if the dialog is cancelable
                .setCancelable(false)
                // positive button text and action
                .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                    dialogCallBack.positiveButtonClick()
                })

            // create dialog box
            val alert = dialogBuilder.create()
            alert.window?.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

            // set title for alert dialog box
            alert.setTitle(title)
            // show alert dialog

            alert.show()

        }

        fun showAlertWithBothButton(
            context: Context,
            title: String,
            message: String,
            dialogCallBack: DialogButtonCallBack
        ) {
            val dialogBuilder = AlertDialog.Builder(context)

            // set message of alert dialog
            dialogBuilder.setMessage(message)
                // if the dialog is cancelable
                .setCancelable(false)
                // positive button text and action
                .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                    dialogCallBack.positiveButtonClick()
                })

                .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                    dialogCallBack.negativeButtonClick()
                })

            // create dialog box
            val alert = dialogBuilder.create()
            // set title for alert dialog box
            alert.setTitle(title)
            // show alert dialog

            alert.show()

        }

        fun showAlertWarningDialog(context: Context, message: String) {
            val dialogBuilder = AlertDialog.Builder(context)

            // set message of alert dialog
            dialogBuilder.setMessage(message)
                // if the dialog is cancelable
                .setCancelable(false)
                // positive button text and action
                .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                })

            // create dialog box
            val alert = dialogBuilder.create()
            // set title for alert dialog box
            alert.setTitle("Warning!")
            // show alert dialog
            alert.show()
        }

        fun showAlertDialogRemarks(context: Context, message: String, activity: Activity) {
            val dialogBuilder = AlertDialog.Builder(context)
            // set message of alert dialog
            dialogBuilder.setMessage(message)
                // if the dialog is cancelable
                .setCancelable(false)
                // positive button text and action
                .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                })
            // create dialog box
            val alert = dialogBuilder.create()
            // set title for alert dialog box
            alert.setTitle("Alert!")
            // show alert dialog
            alert.show()
        }

        fun getImageUri(context: Context, inImage: Bitmap): Uri {
            var bytes = ByteArrayOutputStream()
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
            var path = MediaStore.Images.Media.insertImage(
                context.getContentResolver(),
                inImage,
                "Title",
                null
            );
            return Uri.parse(path)
        }

        fun hideKeyboard(applicationContext: Context, mView: View?) {
            if (mView != null) {
                val imm =
                    applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(mView.windowToken, 0)
            }
        }

        fun isInternetConnection(context: Context): Boolean {
            val cm = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting
        }

        @Throws(IOException::class)
        fun createImageFile(): File {
            // Create an image file name
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
            val imageFileName = "JPEG_" + timeStamp + "_"
            val storageDir = File(
                Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DCIM
                ), "Camera"
            )
            return File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
            )
        }

        fun getImageName(url: String): String {
            return url.substring(url.lastIndexOf('/') + 1, url.length)
        }

        fun checkAndRequestPermission(
            activity: Activity,
            permission: String,
            REQUEST_CODE: Int
        ): Boolean {
            if (ContextCompat.checkSelfPermission(
                    activity,
                    permission
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                return true
            } else {
                ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(permission),
                    REQUEST_CODE
                )
                return false
            }
        }

        fun requestAllPermissions(activity: Activity, permissionCode: Int): Boolean {
            var res: Boolean = false
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // Marshmallow+
                if (ContextCompat.checkSelfPermission(
                        activity,
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                        activity,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                        activity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    activity.requestPermissions(
                        arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), permissionCode
                    )
                } else {
                    res = true
                }
            } else {
                // Pre-Marshmallow
                res = true
            }
            return res
        }

        fun getImageUriFromBitmap(inContext: Context, inImage: Bitmap): Uri {
            val bytes = ByteArrayOutputStream()
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            val path = MediaStore.Images.Media.insertImage(
                inContext.getContentResolver(),
                inImage,
                "Contact",
                null
            );
            return Uri.parse(path);
        }

        fun getRealPathFromURI(contentURI: Uri, context: Context): String {
            var result: String
            var cursor: Cursor =
                context.getContentResolver().query(contentURI, null, null, null, null)!!;
            if (cursor == null) {
                result = contentURI.getPath()!!;
            } else {
                cursor.moveToFirst();
                var idx: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                result = cursor.getString(idx);
                cursor.close();
            }
            return result;
        }

        fun maskString(strText: String, start: Int, end: Int, maskChar: Char): String {
            val idNumberForServer = strText
            var startIndex = start
            var endIndex = end
            if (strText == null || strText.equals(""))
                return "";
            if (startIndex < 0)
                startIndex = 0
            if (endIndex > strText.length)
                endIndex = strText.length
            if (startIndex > endIndex)
                throw  Exception("End index cannot be greater than start index");
            var maskLength = endIndex - start
            if (maskLength == 0)
                return strText;
            var sbMaskString = StringBuilder(maskLength)
            for (i in 1..maskLength) {
                sbMaskString.append(maskChar);
            }
            return strText.substring(
                0,
                start
            ) + sbMaskString.toString() + strText.substring(start + maskLength);
        }

        // Method to save an bitmap to a file
        fun bitmapToFile(context: Context, bitmap: Bitmap): Uri {
            // Get the context wrapper
            val wrapper = ContextWrapper(context)
            // Initialize a new file instance to save bitmap object
            var file = wrapper.getDir("Images", Context.MODE_PRIVATE)
            file = File(file, "${UUID.randomUUID()}.jpg")
            try {
                // Compress the bitmap and save in jpg format
                val stream: OutputStream = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                stream.flush()
                stream.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            // Return the saved bitmap uri
            return Uri.parse(file.absolutePath)
        }

        /*fun saveImage(imageUrl: String, destinationFile: String) {
            val url = URL(imageUrl)
            val ist = url.openStream()
            val os = FileOutputStream(destinationFile)

            val b = ByteArray(2048)
            var length: Int

            while ((length = ist.read(b)) != -1) {
                os.write(b, 0, length)
            }

            ist.close()
            os.close()
        }*/
        fun isVideoFile(path: String): Boolean {
            val mimeType = URLConnection.guessContentTypeFromName(path)
            return mimeType != null && mimeType.startsWith("video")
        }

        @SuppressLint("SimpleDateFormat")
        fun datePicker(context: Context, tv_date_time: TextView): String {
            var date_time = ""
            var date_time1 = ""
            var mYear: Int = 0
            var mMonth: Int = 0
            var mDay: Int = 0
            var date = ""
            var date1 = ""
            val c = Calendar.getInstance()
            mYear = c.get(Calendar.YEAR)
            mMonth = c.get(Calendar.MONTH)
            mDay = c.get(Calendar.DAY_OF_MONTH)
            val datePickerDialog = DatePickerDialog(
                context,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    date_time = (monthOfYear + 1).toString() + " " + dayOfMonth.toString()
                    //*************Call Time Picker Here ********************
                    c.set(year, monthOfYear, dayOfMonth)
                    val format = SimpleDateFormat("MMMM dd ")
                    date = format.format(c.time)
                    val format1 = SimpleDateFormat("mm/dd/yyyy ")
                    date1 = format1.format(c.time)
                    date_time1 =
                        timePicker(context, date, tv_date_time, date1)
                }, mYear, mMonth, mDay
            )
            datePickerDialog.show()
            return date_time1
        }

        fun timePicker(
            context: Context,
            date: String,
            tv_date_time: TextView,
            date1: String
        ): String {
            var dateTime: String = date1
            var mHour: Int = 0
            var mMinute: Int = 0
            // Get Current Time
            val c = Calendar.getInstance()
            mHour = c.get(Calendar.HOUR_OF_DAY)
            mMinute = c.get(Calendar.MINUTE)
            // Launch Time Picker Dialog
            val timePickerDialog = TimePickerDialog(
                context,
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    val ampm = if (hourOfDay < 12) "AM" else "PM"
                    mHour = hourOfDay
                    mMinute = minute
                    dateTime = date1 + " " + hourOfDay + ":" + minute
                    tv_date_time.text = (date + " " + hourOfDay + ":" + minute + " " + ampm)
                }, mHour, mMinute, false
            )
            timePickerDialog.show()
            return dateTime
        }


        interface DialogCallBack {

            fun positiveButtonClick()

        }

        interface DialogButtonCallBack {

            fun positiveButtonClick()
            fun negativeButtonClick()

        }
    }
}