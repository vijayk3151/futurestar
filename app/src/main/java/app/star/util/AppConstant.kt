package app.star.util

object AppConstant{

    val loginScreen="login"
    val BASE_URL="http://api.futurestarr.com/api/v1/"
    var authToken =""
    var selectUserType= 1// 1 for buyer account 2 for Talent Account
    const val temFolder="MyFolder/Images"
    const val deleteTempFolder="MyFolder/Images"
    const val sellerRole= 4
    const val buyerRole= 3
    var userType="simple"// simple , facebook, twitter
}