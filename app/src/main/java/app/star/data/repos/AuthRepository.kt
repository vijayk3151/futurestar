package app.denhan.data.repos

import app.star.data.api.WebService
import app.denhan.helper.DeviceIdHelper
import app.denhan.helper.getStatusCode
import app.star.data.ResponseResource
import app.star.data.repos.ApiResponseCode
import app.star.model.CommonObject
import app.star.model.register.RegisterResponse
import app.star.util.ApiErrorCode
import com.twitter.sdk.android.core.models.ApiError
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response


class  AuthRepository(private val webService: WebService,
                      private val deviceIdHelper: DeviceIdHelper){
    suspend fun registerUser(registerParameter: HashMap<String, String>): ResponseResource<CommonObject<RegisterResponse>?> {
        return try {
            val response: Response<CommonObject<RegisterResponse>> = webService.registerUser(registerParameter)
            val registerResponse = response.body()
            if (response.code() == ApiResponseCode.SUCCESS_CODE) {
                if (registerResponse?.status_code==200) {
                    ResponseResource.success(registerResponse)
                }
                else{
                    ResponseResource.Error<CommonObject<RegisterResponse>>(ApiErrorCode.apiErrorCode, registerResponse?.message?:"")
                }

            } else {
                val jObjError = JSONObject(response.errorBody()?.string())
                ResponseResource.Error<CommonObject<RegisterResponse>>(jObjError.getInt("code"),"Something went wrong")
            }
        } catch (e: Exception) {
            ResponseResource.Error<CommonObject<RegisterResponse>>(e.getStatusCode(),e.printStackTrace().toString())
        }
    }

    suspend fun loginUser(loginparameter: HashMap<String, String>): ResponseResource<CommonObject<RegisterResponse>?> {
        return try {
            val response: Response<CommonObject<RegisterResponse>> = webService.registerUser(loginparameter)
            val registerResponse = response.body()
            if (response.code() == ApiResponseCode.SUCCESS_CODE) {
                if (registerResponse?.status_code==200) {
                    ResponseResource.success(registerResponse)
                }
                else{
                    ResponseResource.Error<CommonObject<RegisterResponse>>(ApiErrorCode.apiErrorCode, registerResponse?.message?:"")
                }

            } else {
                val jObjError = JSONObject(response.errorBody()?.string())
                ResponseResource.Error<CommonObject<RegisterResponse>>(jObjError.getInt("code"),"Something went wrong")
            }
        } catch (e: Exception) {
            ResponseResource.Error<CommonObject<RegisterResponse>>(e.getStatusCode(),e.printStackTrace().toString())
        }
    }

    suspend fun forgotPassword(forgotParameter: HashMap<String, String>): ResponseResource<CommonObject<*>?> {
        return try {
            val response: Response<CommonObject<*>> = webService.forgotPassword(forgotParameter)
            val registerResponse = response.body()
            if (response.code() == ApiResponseCode.SUCCESS_CODE) {
                if (registerResponse?.status_code==200) {
                    ResponseResource.success(registerResponse)
                }
                else{
                    ResponseResource.Error<CommonObject<RegisterResponse>>(ApiErrorCode.apiErrorCode, registerResponse?.message?:"")
                }

            } else {
                val jObjError = JSONObject(response.errorBody()?.string())
                ResponseResource.Error<CommonObject<*>>(jObjError.getInt("code"),"Something went wrong")
            }
        } catch (e: Exception) {
            ResponseResource.Error<CommonObject<*>>(e.getStatusCode(),e.printStackTrace().toString())
        }
    }



}







