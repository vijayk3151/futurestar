package app.star.data

sealed class ResponseResource<out T> {

    class Loading<T> : ResponseResource<T>()

    data class Success<T>(val value: T) : ResponseResource<T>()

    data class Error<T>(val code: Int,val message:String) : ResponseResource<T>()

    val isLoading: Boolean get() = this is ResponseResource.Loading

    val isSuccess: Boolean get() = this is ResponseResource.Success

    val isError: Boolean get() = this is ResponseResource.Error


    companion object {

        fun <T> loading() = ResponseResource.Loading<T>()

        fun <T> success(value: T) = ResponseResource.Success(value)

        fun <T> error(code: Int, message: String) = ResponseResource.Error<T>(code,message)
    }
}