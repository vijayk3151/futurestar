package app.star.data.api

import app.star.model.CommonObject
import app.star.model.register.RegisterResponse
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST


interface WebService {

    @POST("user")
    suspend fun registerUser(@Body registerParameter: Map<String, String>):
            Response<CommonObject<RegisterResponse>>

    @POST("user/login")
    suspend fun loginUser(@Body registerParameter: Map<String, String>):
            Response<CommonObject<RegisterResponse>>

    @POST("user/forgot-password")
    suspend fun forgotPassword(@Body registerParameter: Map<String, String>):
            Response<CommonObject<*>>

}