package app.star.activity.register

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.denhan.data.repos.AuthRepository
import app.star.R
import app.star.data.ResponseResource
import app.star.module.ResourceProvider
import app.star.module.SealedClass
import app.star.util.AppConstant
import app.star.util.CommonMethod
import app.star.util.SharedKey
import app.star.util.SharedKey.email
import app.star.util.SharedKey.first_name
import app.star.util.SharedKey.last_name
import app.star.util.SharedKey.password
import app.star.util.SharedKey.role
import app.star.util.SharedKey.user_name
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class RegisterViewModel(private val authRepository: AuthRepository,
                        private val resourceProvider: ResourceProvider) :ViewModel(){
    val emailString = MutableLiveData<String>()
    val passwordString = MutableLiveData<String>()
    val firstName = MutableLiveData<String>()
    val  userName = MutableLiveData<String>()
    val  lastName = MutableLiveData<String>()
    val  confirmPassword = MutableLiveData<String>()

    val emailErrorString = MediatorLiveData<String>()
    val passwordErrorString = MediatorLiveData<String>()
    val confirmPasswordErrorString = MediatorLiveData<String>()
    val firstNameErrorString = MediatorLiveData<String>()
    val userNameErrorString = MediatorLiveData<String>()

    val emailValid = MutableLiveData<Boolean>()
    val passwordValid = MutableLiveData<Boolean>()
    val confirmPasswordValid = MutableLiveData<Boolean>()
    val firstNameValid = MutableLiveData<Boolean>()
    val userNameValid = MutableLiveData<Boolean>()
    val isShowError = MutableLiveData<Boolean>()

    val textTittle= MutableLiveData<String>()
    val buttonText= MutableLiveData<String>()
    val fieldVisibility=MutableLiveData<Boolean>()




    var singleEventUiState = MutableLiveData<SealedClass>()

    init {

        setViewState()
        emailValid.postValue(false)
        passwordValid.postValue(false)
        emailErrorString.addSource(emailString) { validateEmail() }
        passwordErrorString.addSource(passwordString) { validatePassword() }
        confirmPasswordErrorString.addSource(confirmPassword) { validateConfirmPassword() }
        userNameErrorString.addSource(userName){validateUserName()}
        firstNameErrorString.addSource(firstName){validateFirstName()}
    }

    fun setViewState(){
        when(AppConstant.selectUserType){
            1->{
                fieldVisibility.value=true
                textTittle.postValue(resourceProvider.getStringResource(R.string.register_as_buyer_text))
                buttonText.postValue(resourceProvider.getStringResource(R.string.register_text))

            }
            2->{
                fieldVisibility.value= false
                buttonText.postValue(resourceProvider.getStringResource(R.string.create_talent_account_text))
                textTittle.postValue(resourceProvider.getStringResource(R.string.register_as_talent_text))
            }

        }
    }

    private fun validateFirstName() {
        val firstName = firstName.value?:""
        if (CommonMethod.notEmptyFieldValidation(firstName)){
            firstNameErrorString.postValue("")
            firstNameValid.postValue(true)
        } else{
            firstNameValid.postValue(false)
            firstNameErrorString.postValue(resourceProvider.getStringResource(R.string.required_error))

        }
    }

    private fun validateUserName() {
        val userName = userName.value?:""
        if (CommonMethod.notEmptyFieldValidation(userName)){
            userNameErrorString.postValue("")
            userNameValid.postValue(true)
        }
        else{
            userNameValid.postValue(false)
            userNameErrorString.postValue(resourceProvider.getStringResource(R.string.required_error))

        }
    }

    private fun validateEmail() {
        val email = emailString.value ?: ""
        when {
            email.isEmpty() -> {
                emailValid.postValue(false)
                emailErrorString.postValue(resourceProvider.getStringResource(R.string.invalid_email_error))
            }
            CommonMethod.isEmailValid(email).not() -> {
                emailValid.postValue(false)
                emailErrorString.postValue(resourceProvider.getStringResource(R.string.invalid_email_error))
            }
            else -> {
                emailErrorString.postValue("")
                emailValid.postValue(true)

            }
        }
    }

    /* validate the  password here
    * */
    private fun validatePassword() {
        val code = passwordString.value?.trim()
        when {
            code.isNullOrEmpty() -> {
                passwordValid.postValue(false)
                passwordErrorString.postValue(resourceProvider.getStringResource(R.string.required_error))
            }
            code.length < 7 -> {
                passwordValid.postValue(false)
                passwordErrorString.postValue(resourceProvider.getStringResource(R.string.password_length_error))
            }
            else -> {
                passwordValid.postValue(true)
                passwordErrorString.postValue("")
            }
        }
    }

    /* validate the  password here
  * */
    private fun validateConfirmPassword() {
        val password = passwordString.value?.trim()
        val confirmPassword = confirmPassword.value?.trim()
        when {
            confirmPassword.isNullOrEmpty() -> {
                confirmPasswordValid.postValue(false)
                confirmPasswordErrorString.postValue(resourceProvider.getStringResource(R.string.required_error))
            }
            password!=confirmPassword->{
                confirmPasswordValid.postValue(false)
                confirmPasswordErrorString.postValue(resourceProvider.getStringResource(R.string.confirm_password_error))
            }
            else -> {
                confirmPasswordValid.postValue(true)
                confirmPasswordErrorString.postValue("")
            }
        }
    }

    private fun isRegisterData(): Boolean {
        return emailValid.value ?: false && passwordValid.value ?: false && firstNameValid.value?:false
                &&userNameValid.value?:false && confirmPasswordValid.value?:false
    }


    fun onClickRegisterButton() {
        isShowError.postValue(true)
        if (isRegisterData()) {
            isShowError.postValue(false)
            val registerParameter = HashMap<String,String>()
            registerParameter.put(first_name,firstName.value?:"")
            registerParameter.put(last_name, lastName.value?:"")
            registerParameter.put(user_name, userName.value?:"")
            registerParameter.put(email, emailString.value?:"")
            registerParameter.put(password, passwordString.value?:"")
            if (AppConstant.selectUserType==1) {
                registerParameter.put(role, AppConstant.buyerRole.toString())
            }
            else{
                registerParameter.put(role, AppConstant.sellerRole.toString())
            }
            hitRegisterApi(registerParameter)
        } else {
            if (userNameValid.value==false){
                userNameErrorString.postValue(resourceProvider.getStringResource(R.string.required_error))
            }
            if (confirmPasswordValid.value==false){
                confirmPasswordErrorString.postValue(resourceProvider.getStringResource(R.string.confirm_password_error))
            }

            if (firstNameValid.value ==false){
                firstNameErrorString.postValue(resourceProvider.getStringResource(R.string.required_error))

            }
            if (emailValid.value == false) {
                emailErrorString.postValue(resourceProvider.getStringResource(R.string.invalid_email_error))
            }
            if (passwordValid.value == false) {
                if (passwordString.value?.isEmpty() ?: false) {
                    passwordErrorString.postValue(resourceProvider.getStringResource(R.string.required_error))
                } else {
                    passwordErrorString.postValue(resourceProvider.getStringResource(R.string.password_length_error))

                }
            }

        }

    }


    private fun hitRegisterApi(registerParameter:HashMap<String,String>){
        singleEventUiState.postValue(SealedClass.Loading)
        GlobalScope.launch {
            when (val resource = authRepository.registerUser(registerParameter)) {
                is ResponseResource.Success -> {
                    singleEventUiState.postValue(SealedClass.DismissLoading)
                    singleEventUiState.postValue(SealedClass.HasData(resource.value?.data))

                }
                is ResponseResource.Error -> {
                    singleEventUiState.postValue(SealedClass.DismissLoading)
                    singleEventUiState.postValue(SealedClass.ErrorClass(resource.message))
                }
            }
        }
    }
}