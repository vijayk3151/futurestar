package app.star.activity.register

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.transition.Slide
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.star.R
import app.star.activity.base.BaseActivity
import app.star.activity.home.HomeActivity
import app.star.activity.login.LoginActivity
import app.star.databinding.ActivityRegisterBinding
import app.star.module.SealedClass
import app.star.util.AppConstant
import app.star.util.CommonMethod
import app.star.util.DialogCallBack
import org.koin.android.viewmodel.ext.android.viewModel
import skycap.android.core.livedata.observeNonNull

class RegisterActivity : BaseActivity() {
    lateinit var binding:ActivityRegisterBinding
    private val viewModel: RegisterViewModel by viewModel()
    var popupWindow : PopupWindow?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        binding.viewModel=viewModel
        binding.lifecycleOwner=this
        intiView()

    }

    private fun intiView() {
        viewModel.setViewState()
        observeLiveData()
        clickEvent()
    }

    private fun clickEvent() {

        binding.infoIcon.setOnClickListener {
            if (popupWindow!=null){
                if (popupWindow?.isShowing?:false){
                    popupWindow?.dismiss()
                }
                else{
                    showPopWindow(binding.infoIcon)
                }

            }
            else{
                showPopWindow(binding.infoIcon)
            }


        }
    }

    private fun observeLiveData(){
        observeNonNull(viewModel.singleEventUiState){
            when(it){
                is SealedClass.Loading->{
                    showProgressDialog()
                }
                is SealedClass.DismissLoading->{
                    hideProgressDialog()
                }
                is SealedClass.StartNextScreen->{
                    when(it.targetScreen){
                        AppConstant.loginScreen->{
                            CommonMethod.showSuccessDialog(this,getString(R.string.register_success_message),object :DialogCallBack{
                                override fun positiveClick() {
                                    startSpecificActivity(LoginActivity::class.java)
                                }
                            })


                        }

                    }
                    //  startLoginScreen()
                }
                is SealedClass.HasData<*>->{

                }
                is SealedClass.ErrorClass ->{
                    hideProgressDialog()
                    CommonMethod.showAlertDialog(
                        this,it.errorMessage
                    )
                }
            }
        }
    }

    fun showPopWindow(view1: View?) { // inflate the layout of the popup window
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        // Inflate a custom view using layout inflater
        val view = inflater.inflate(R.layout.buyer_register_alert,null)
        val width = binding.txtView1.measuredWidth
        // Initialize a new instance of popup window
        popupWindow = PopupWindow(
            view, // Custom view to show in popup window
            LinearLayout.LayoutParams.MATCH_PARENT, // Width of popup window
            LinearLayout.LayoutParams.WRAP_CONTENT // Window height
        )

        // Set an elevation for the popup window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow?.elevation = 10.0F
        }


        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            popupWindow?.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.TOP
            popupWindow?.exitTransition = slideOut
            popupWindow?.isOutsideTouchable=true

        }



        popupWindow?.showAsDropDown(binding.txtView1,15,15)

    }
}
