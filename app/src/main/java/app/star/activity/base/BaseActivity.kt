package app.star.activity.base

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import app.star.R
import app.star.util.CallProgressWheel
import app.star.util.CommonMethods
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import gun0912.tedbottompicker.TedBottomPicker

open class BaseActivity :AppCompatActivity(){
    lateinit var context:Context
    var dialog: Dialog?=null
    lateinit var builder: AlertDialog.Builder
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this
        supportActionBar?.hide()
    }

    fun isNetworkConnected(): Boolean {
        return CommonMethods.isInternetConnection(context)
    }


    fun showProgressDialog() {

        if (dialog?.isShowing?:false){
            dialog?.dismiss()
        }
        dialog = Dialog(context)
        val inflate = LayoutInflater.from(context).inflate(R.layout.layout_progress, null)
        dialog?.setContentView(inflate)
        dialog?.setCancelable(false)
        dialog?.window!!.setBackgroundDrawable(
            ColorDrawable(Color.TRANSPARENT)
        )
        dialog?.show()
        // CallProgressWheel.showLoadingDialog(context)
    }



    fun hideProgressDialog() {
        try {
            if (dialog != null && dialog?.isShowing?:false) {
                dialog?.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        /*if (CallProgressWheel.isDialogShowing) {
            CallProgressWheel.dismissLoadingDialog()
        }*/
    }
    open fun permissionForImage(imageCallBack: ImageCallBack) {
        val permissionlistener: PermissionListener = object : PermissionListener {
            override fun onPermissionGranted() {
                showImageCapturing(imageCallBack)
            }

            override fun onPermissionDenied(deniedPermissions: List<String>) {
                imageCallBack.deniedPermission("Please give the permission for accessing the images")
            }
        }
        TedPermission.with(this)
            .setPermissionListener(permissionlistener)
            .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
            .setPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .check()
    }
    open fun showImageCapturing(imageCallBack: ImageCallBack) {
        TedBottomPicker.with(this)
            .show {
                imageCallBack.returnImageUri(it)
            }
    }
    fun hideKeyboard(applicationContext: Context, mView: View?) {
        if (mView != null) {
            val imm = applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(mView.windowToken, 0)
        }
    }
    override fun onDestroy() {
        hideProgressDialog()
        super.onDestroy()
    }

    open fun startSpecificActivity(otherActivityClass: Class<*>?) {
        val intent = Intent(applicationContext, otherActivityClass)
        startActivity(intent)
    }

    open fun addOrReplaceFragmentName(fragmentName: Fragment, container: Int){
        supportFragmentManager
            .beginTransaction()
            .replace(container, fragmentName)
            .addToBackStack(fragmentName.tag)
            .commit()


    }


    /*open fun logOutUser(){
        val intent = Intent(applicationContext, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }*/

}
interface ImageCallBack{
    fun deniedPermission(errorMessage: String)
    fun returnImageUri(imageUri: Uri)
}