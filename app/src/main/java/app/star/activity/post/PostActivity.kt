package app.star.activity.post

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ScrollView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import app.star.R
import app.star.activity.base.BaseActivity
import app.star.activity.base.ImageCallBack
import app.star.adapter.PostImagesAdapter
import app.star.databinding.ActivityPostBinding
import app.star.util.ImageCompress
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File

class PostActivity : BaseActivity() {
    lateinit var binding: ActivityPostBinding
    private val viewModel: PostViewModel by viewModel()
    lateinit var postImagesAdapter: PostImagesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post)
        binding.viewModel = viewModel
        binding.lifecycleOwner=this
        intiView()

    }

    private fun intiView() {

        setAdapter()
    }

    private fun setAdapter() {
        viewModel.imageArray = ArrayList()
        viewModel.imageArray.add("Add More")
        postImagesAdapter  = PostImagesAdapter(viewModel.imageArray,object : PostImagesAdapter.ImageAdapterListener{
            override fun onItemClickListener(selectedItem: String) {

            }
            override fun addImage() {
                loadImage()
            }
        })
        binding.imageList.layoutManager= LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.imageList.adapter= postImagesAdapter

    }

    fun loadImage(){
        permissionForImage(object: ImageCallBack {
            override fun returnImageUri(it: Uri) {
                val imageUri= ImageCompress.compressImagePath(it.toString(), this@PostActivity)
                val compressImage= Uri.fromFile( File(imageUri))
                viewModel.imageArray.add(0,compressImage.toString())
                postImagesAdapter.notifyAdapter(viewModel.imageArray)
                binding.imageList.smoothScrollToPosition(viewModel.imageArray.size-1)

            }override fun deniedPermission(message: String) {

            }
        })
    }
}
