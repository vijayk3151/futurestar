package app.star.activity.post

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.denhan.data.repos.AuthRepository
import app.star.module.ResourceProvider

class PostViewModel(private val authRepository: AuthRepository,
                    private val resourceProvider: ResourceProvider) :ViewModel(){
    var captionStr= MutableLiveData<String>()
    var tagPeople= MutableLiveData<String>()
    var locationAdded= MutableLiveData<String>()
    var selectedProduct= MutableLiveData<String>()
    var imageArray = ArrayList<String>()


    init {

    }

    fun postButtonClick(){

    }
}