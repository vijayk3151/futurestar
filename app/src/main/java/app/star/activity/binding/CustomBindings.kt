package app.star.activity.binding
import android.view.View
import android.widget.EditText
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData

        @BindingAdapter("error")
        fun EditText.setError(errorMessage: String?) {
            if (errorMessage!="") {
                error = errorMessage
            }
            else{
                error=""
            }
        }
        @BindingAdapter("visibility")
        fun View.setVisibility( value:MutableLiveData<Boolean>){
            //setVisibleOrGone(value.value)
        }




