    package app.star.activity.home

    import android.os.Bundle
    import android.view.MenuItem
    import androidx.databinding.DataBindingUtil
    import app.star.R
    import app.star.activity.base.BaseActivity
    import app.star.databinding.ActivityHomeBinding
    import app.star.fragment.mall.TalentMallFragment
    import app.star.fragment.search.StarSearchFragment
    import app.star.fragment.trending.TrendingFragment

    class HomeActivity : BaseActivity() {
        lateinit var binding: ActivityHomeBinding

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
            intiView()
        }

        private fun intiView() {
            clickEvent()
            addOrReplaceFragmentName(TrendingFragment(), R.id.container)
        }

        private fun clickEvent() {

            binding.bottomNavigation.setOnNavigationItemSelectedListener { item: MenuItem ->
                return@setOnNavigationItemSelectedListener when (item.itemId) {
                    R.id.home_tab -> {
                        addOrReplaceFragmentName(TrendingFragment(), R.id.container)
                        true
                    }
                    R.id.search_tab->{
                        addOrReplaceFragmentName(StarSearchFragment(), R.id.container)
                        true
                    }
                    else -> false
                }
            }
        }
    }


