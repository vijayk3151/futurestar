package app.star.activity.accountselection

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.star.R
import app.star.activity.base.BaseActivity
import app.star.activity.register.RegisterActivity
import app.star.databinding.ActivityAccountSelectionBinding
import app.star.util.AppConstant
import app.star.util.AppConstant.selectUserType

class AccountSelection : BaseActivity() {
    lateinit var binding: ActivityAccountSelectionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_account_selection)
        intiView()

    }

    private fun intiView() {
        clickEvent()
    }

    private fun clickEvent() {
        binding.buyerButton.setOnClickListener {
            selectUserType=1
            startSpecificActivity(RegisterActivity::class.java)
        }
        binding.sellerButton.setOnClickListener {
            selectUserType=2
            startSpecificActivity(RegisterActivity::class.java)
        }
    }

}
