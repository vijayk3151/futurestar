package app.star.activity.login

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import app.star.R
import app.star.activity.base.BaseActivity
import app.star.activity.home.HomeActivity
import app.star.databinding.ActivityLoginBinding
import app.star.module.SealedClass
import app.star.util.CommonMethod
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.shantanudeshmukh.linkedinsdk.LinkedInBuilder
import com.shantanudeshmukh.linkedinsdk.helpers.LinkedInUser
import com.shantanudeshmukh.linkedinsdk.helpers.OnBasicProfileListener
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import org.koin.android.viewmodel.ext.android.viewModel
import skycap.android.core.livedata.observeNonNull
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*


class LoginActivity : BaseActivity() ,LoginInterface{
    private val LINKEDIN_REQUEST_CODE: Int = 100;
    lateinit var binding: ActivityLoginBinding
    lateinit var callBackManager: CallbackManager
    lateinit var accessToken: AccessToken
    lateinit var graphRequest: GraphRequest
    lateinit var twitterApiClient: TwitterAuthClient
    var buttonStatus = 1// 1 for facebook, 2 for twitter 3
    val TAG = "LoginActivity=>"
    private val viewModel: LoginViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val mTwitterAuthConfig = TwitterAuthConfig(
            getString(R.string.twitter_consumer_key),
            getString(R.string.twitter_consumer_secret)
        )
        val twitterConfig = TwitterConfig.Builder(this)
            .twitterAuthConfig(mTwitterAuthConfig)
            .debug(true)
            .build()
        Twitter.initialize(twitterConfig)
//        initLinkdin()
        //  twitterLogin()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.viewModel=viewModel
        binding.handler=this
        binding.lifecycleOwner=this
        binding.executePendingBindings()
        observeLiveData()
        generete()
        intiView()

    }

    /*intiView=> stating function of the class
    * */
    private fun intiView() {
        callBackManager = CallbackManager.Factory.create()
        clickEvent()
    }

    private fun clickEvent() {
        binding.facebookLogin.setOnClickListener {
            buttonStatus = 1
            faceBookLogin()
        }
        binding.twitterLogin.setOnClickListener {
            buttonStatus = 2
            twitterLoginFunction()
        }
//        binding.linkdinLogin.setOnClickListener {
//            buttonStatus = 3
//        }
        binding.loginButton.setOnClickListener {
            startSpecificActivity(HomeActivity::class.java)
        }

    }

    fun generete() {
        val info: PackageInfo
        try {
            info = packageManager.getPackageInfo("app.star", PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                var md: MessageDigest
                md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val something = String(Base64.encode(md.digest(), 0))
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something)
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("no such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("exception", e.toString())
        }
    }

    private fun twitterLoginFunction() {
        twitterApiClient = TwitterAuthClient()
        twitterApiClient.authorize(this@LoginActivity, object : Callback<TwitterSession?>() {
            override fun success(twitterSessionResult: Result<TwitterSession?>?) {
                twitterApiClient.requestEmail(twitterSessionResult?.data,object:Callback<String>(){
                    override fun success(result: Result<String>?) {
                        Toast.makeText(this@LoginActivity, "your email id ${result?.data}", Toast.LENGTH_SHORT).show()
                    }

                    override fun failure(exception: TwitterException?) {
                        Toast.makeText(this@LoginActivity, exception?.message, Toast.LENGTH_SHORT).show()
                    }

                })
//                Toast.makeText(this@LoginActivity, "success", Toast.LENGTH_SHORT).show()
            }

            override fun failure(e: TwitterException?) {
                Toast.makeText(this@LoginActivity, "failure", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun twitterLogin() {
        val mTwitterAuthConfig = TwitterAuthConfig(
            getString(R.string.twitter_consumer_key),
            getString(R.string.twitter_consumer_secret)
        )
        val twitterConfig = TwitterConfig.Builder(this)
            .twitterAuthConfig(mTwitterAuthConfig)
            .debug(true)//enable debug mode
            .build()
        Twitter.initialize(twitterConfig)

    }


    private fun faceBookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(
            this,
            Arrays.asList("user_photos", "email", "public_profile", "user_posts")
        )
        LoginManager.getInstance()
            .logInWithPublishPermissions(this, Arrays.asList("publish_actions"))
        LoginManager.getInstance().registerCallback(callBackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) { // App code
                    Log.e(TAG, loginResult?.accessToken?.token ?: "")
                    makeGraphRequest(loginResult?.accessToken)
                }

                override fun onCancel() { // App code
                    Log.e(TAG, " Cancel")
                }

                override fun onError(exception: FacebookException) { // App code
                    Log.e(TAG, exception.message ?: "")
                }
            })

    }

    private fun makeGraphRequest(accessToken: AccessToken?) {
        val request = GraphRequest.newMeRequest(
            accessToken
        ) { `object`, response ->
            // Application code
            Toast.makeText(this@LoginActivity, response.jsonObject.get("name").toString(), Toast.LENGTH_SHORT).show()
        }
        val parameters = Bundle()
        parameters.putString("fields", "id,name,link")
        request.parameters = parameters
        request.executeAsync()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == LINKEDIN_REQUEST_CODE) {
            data?.let {
                val linkdinData = it.getParcelableArrayExtra("social_login") as LinkedInUser
                getLinkdinProfileData(linkdinData)
            }
        }
        when (buttonStatus) {
            1 -> {
                callBackManager.onActivityResult(requestCode, resultCode, data)
            }
            2 -> {
                twitterApiClient.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private fun getLinkdinProfileData(linkedInUser: LinkedInUser) {
        LinkedInBuilder.retrieveBasicProfile(
            linkedInUser.accessToken,
            linkedInUser.accessTokenExpiry,
            object : OnBasicProfileListener {
                override fun onDataRetrievalStart() {
                    //show progress bar if required
                }

                override fun onDataSuccess(linkedInUser: LinkedInUser) {
                    //handle retrived data
                    Log.e(TAG, "onDataSuccess: ${linkedInUser.email}" )
                }

                override fun onDataFailed(errCode: Int, errMessage: String) {
                    //handle error
                }
            })
    }

    private fun observeLiveData() {
        observeNonNull(viewModel.singleEventUiState) {
            when (it) {
                is SealedClass.Loading -> {
                    showProgressDialog()
                }
                is SealedClass.DismissLoading -> {
                    hideProgressDialog()
                }

                is SealedClass.HasData<*> -> {

                }
                is SealedClass.ErrorClass -> {
                    hideProgressDialog()
                    CommonMethod.showAlertDialog(
                        this, it.errorMessage
                    )
                }
            }
        }
        observeNonNull(viewModel.onLinkdinClick){
            if(it) Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show()
        }
    }

    private  fun initLinkdin() {
        LinkedInBuilder.getInstance(this)
            .setClientID(getString(R.string.linkdin_client_id))
            .setClientSecret(getString(R.string.linkdin_client_secret))
            .setRedirectURI(getString(R.string.redirecting_url))
            .authenticate(LINKEDIN_REQUEST_CODE)
    }

    override fun onClickLinkin() {
       buttonStatus=3
        initLinkdin()
    }
}


