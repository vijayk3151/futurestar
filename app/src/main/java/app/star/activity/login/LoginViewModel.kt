package app.star.activity.login

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.denhan.data.repos.AuthRepository
import app.star.R
import app.star.data.ResponseResource
import app.star.module.ResourceProvider
import app.star.module.SealedClass
import app.star.util.AppConstant
import app.star.util.CommonMethod
import app.star.util.SharedKey
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LoginViewModel(private val authRepository: AuthRepository,
                     private val resourceProvider: ResourceProvider) :ViewModel(){
    val emailString = MutableLiveData<String>()
    val passwordString = MutableLiveData<String>()
    val emailErrorString = MediatorLiveData<String>()
    val passwordErrorString = MediatorLiveData<String>()


    val emailValid = MutableLiveData<Boolean>()
    val passwordValid = MutableLiveData<Boolean>()



    val isShowError = MutableLiveData<Boolean>()
    var singleEventUiState = MutableLiveData<SealedClass>()

    val onLinkdinClick=MutableLiveData<Boolean>(false)

    init {

        emailValid.postValue(false)
        passwordValid.postValue(false)
        emailErrorString.addSource(emailString) { validateEmail() }
        passwordErrorString.addSource(passwordString) { validatePassword() }
    }

    fun onClickFacebook(){

    }

    fun onclickOnLogin(){
        isShowError.postValue(true)
        if (isLoginData()) {
            isShowError.postValue(false)
            val loginParameter = HashMap<String,String>()
            loginParameter.put(SharedKey.email, emailString.value?:"")
            loginParameter.put(SharedKey.password, passwordString.value?:"")
            hitLoginParameter(loginParameter)
        } else {

            if (emailValid.value == false) {
                emailErrorString.postValue(resourceProvider.getStringResource(R.string.invalid_email_error))
            }
            if (passwordValid.value == false) {
                if (passwordString.value?.isEmpty() ?: false) {
                    passwordErrorString.postValue(resourceProvider.getStringResource(R.string.required_error))
                } else {
                    passwordErrorString.postValue(resourceProvider.getStringResource(R.string.password_length_error))

                }
            }

        }

    }
    private fun hitRegister(registerParameter:HashMap<String,String>){
        singleEventUiState.postValue(SealedClass.Loading)
        GlobalScope.launch {
            when (val resource = authRepository.registerUser(registerParameter)) {
                is ResponseResource.Success -> {
                    singleEventUiState.postValue(SealedClass.DismissLoading)
                    singleEventUiState.postValue(SealedClass.HasData(resource.value?.data))

                }
                is ResponseResource.Error -> {
                    singleEventUiState.postValue(SealedClass.DismissLoading)
                    singleEventUiState.postValue(SealedClass.ErrorClass(resource.message))
                }
            }
        }
    }

    private fun hitLoginParameter(loginParameter:HashMap<String,String>){
        singleEventUiState.postValue(SealedClass.Loading)
        GlobalScope.launch {
            when (val resource = authRepository.loginUser(loginParameter)) {
                is ResponseResource.Success -> {
                    singleEventUiState.postValue(SealedClass.DismissLoading)
                    singleEventUiState.postValue(SealedClass.HasData(resource.value?.data))

                }
                is ResponseResource.Error -> {
                    singleEventUiState.postValue(SealedClass.DismissLoading)
                    singleEventUiState.postValue(SealedClass.ErrorClass(resource.message))
                }
            }
        }
    }
    private fun validateEmail() {
        val email = emailString.value ?: ""
        when {
            email.isEmpty() -> {
                emailValid.postValue(false)
                emailErrorString.postValue(resourceProvider.getStringResource(R.string.invalid_email_error))
            }
            CommonMethod.isEmailValid(email).not() -> {
                emailValid.postValue(false)
                emailErrorString.postValue(resourceProvider.getStringResource(R.string.invalid_email_error))
            }
            else -> {
                emailErrorString.postValue("")
                emailValid.postValue(true)

            }
        }
    }

    /* validate the  password here
    * */
    private fun validatePassword() {
        val code = passwordString.value?.trim()
        when {
            code.isNullOrEmpty() -> {
                passwordValid.postValue(false)
                passwordErrorString.postValue(resourceProvider.getStringResource(R.string.required_error))
            }
            code.length < 7 -> {
                passwordValid.postValue(false)
                passwordErrorString.postValue(resourceProvider.getStringResource(R.string.password_length_error))
            }
            else -> {
                passwordValid.postValue(true)
                passwordErrorString.postValue("")
            }
        }
    }

    private fun isLoginData(): Boolean {
        return emailValid.value ?: false && passwordValid.value ?: false
    }

     fun onClickLinkdin(){
        onLinkdinClick.postValue(true)
    }

}
interface  LoginInterface{
    fun onClickLinkin()
}