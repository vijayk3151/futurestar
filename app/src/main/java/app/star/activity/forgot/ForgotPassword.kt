package app.star.activity.forgot

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.star.R
import app.star.activity.base.BaseActivity
import app.star.activity.login.LoginActivity
import app.star.activity.login.LoginViewModel
import app.star.databinding.ActivityForgotPasswordBinding
import app.star.model.CommonObject
import app.star.module.SealedClass
import app.star.util.AppConstant
import app.star.util.CommonMethod
import app.star.util.DialogCallBack
import org.koin.android.viewmodel.ext.android.viewModel
import skycap.android.core.livedata.observeNonNull

class ForgotPassword : BaseActivity() {
    lateinit var binding: ActivityForgotPasswordBinding
    private val viewModel: ForgotViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_forgot_password)
        binding.viewModel= viewModel
        binding.lifecycleOwner= this
        intiView()

    }

    private fun intiView() {
        observeLiveData()
    }

    private fun observeLiveData(){
        observeNonNull(viewModel.singleEventUiState){
            when(it){
                is SealedClass.Loading->{
                    showProgressDialog()
                }
                is SealedClass.DismissLoading->{
                    hideProgressDialog()
                }

                is SealedClass.HasData<*>->{
                    var responseObject = it.content as CommonObject<*>
                    CommonMethod.showSuccessDialog(
                        this,
                        responseObject.message,
                        object :
                            DialogCallBack {
                            override fun positiveClick() {
                               finish()
                            }
                        })





                }
                is SealedClass.ErrorClass ->{
                    hideProgressDialog()
                    CommonMethod.showAlertDialog(
                        this,it.errorMessage
                    )
                }
            }
        }
    }


}
