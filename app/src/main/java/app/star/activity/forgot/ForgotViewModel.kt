package app.star.activity.forgot

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.denhan.data.repos.AuthRepository
import app.star.R
import app.star.data.ResponseResource
import app.star.module.ResourceProvider
import app.star.module.SealedClass
import app.star.util.CommonMethod
import app.star.util.SharedKey
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ForgotViewModel (private val authRepository: AuthRepository,
                       private val resourceProvider: ResourceProvider): ViewModel(){

    var forgotEmailError= MediatorLiveData<String>()
    var forgotEmail= MutableLiveData<String>()
    var forgotEmailValid= MutableLiveData<Boolean>()

    val isShowError = MutableLiveData<Boolean>()
    var singleEventUiState = MutableLiveData<SealedClass>()


    init {

        forgotEmailValid.postValue(false)
        forgotEmailError.addSource(forgotEmail) { validateEmail() }

    }

    fun onClickOnForgotPassword(){
        isShowError.postValue(true)
        when (forgotEmailValid.value) {
            true -> {
                isShowError.postValue(false)
                val forgotParameter = HashMap<String,String>()
                forgotParameter[SharedKey.email] = forgotEmail.value?:""

                hitForgotPassword(forgotParameter)

            }
            false->{
                if (forgotEmailValid.value == false) {
                    forgotEmailError.postValue(resourceProvider.getStringResource(R.string.invalid_email_error))
                }
            }
        }

    }

    private fun hitForgotPassword(forgotParameter:HashMap<String,String>){
        singleEventUiState.postValue(SealedClass.Loading)
        GlobalScope.launch {
            when (val resource = authRepository.forgotPassword(forgotParameter)) {
                is ResponseResource.Success -> {
                    singleEventUiState.postValue(SealedClass.DismissLoading)
                    singleEventUiState.postValue(SealedClass.HasData(resource.value?.data))
                }
                is ResponseResource.Error -> {
                    singleEventUiState.postValue(SealedClass.DismissLoading)
                    singleEventUiState.postValue(SealedClass.ErrorClass(resource.message))
                }
            }
        }
    }




    private fun validateEmail() {
        val email = forgotEmail.value ?: ""
        when {
            email.isEmpty() -> {
                forgotEmailValid.postValue(false)
                forgotEmailError.postValue(resourceProvider.getStringResource(R.string.invalid_email_error))
            }
            CommonMethod.isEmailValid(email).not() -> {
                forgotEmailValid.postValue(false)
                forgotEmailError.postValue(resourceProvider.getStringResource(R.string.invalid_email_error))
            }
            else -> {
                forgotEmailError.postValue("")
                forgotEmailValid.postValue(true)

            }
        }
    }

}

