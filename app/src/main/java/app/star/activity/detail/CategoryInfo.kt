package app.star.activity.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import app.star.R
import app.star.activity.base.BaseActivity
import app.star.activity.home.HomeActivity
import app.star.activity.post.PostActivity
import app.star.adapter.CategoryDetailAdapter
import app.star.adapter.TrendingAdapter
import app.star.databinding.CategoryDetailBinding

class CategoryInfo : BaseActivity() {
    lateinit var binding: CategoryDetailBinding
    lateinit var starList: ArrayList<String>
    lateinit var categoryDetailAdapter: CategoryDetailAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.category_detail)
        intiView()
    }

    private fun intiView() {
        starList= ArrayList()
        starList.add("cricket")
        starList.add("cricket")
        starList.add("cricket")
        starList.add("cricket")
        setListAdapter()
        clickEvent()
    }

    private fun clickEvent() {

        binding.postLayout.setOnClickListener {
            binding.postLayout.setBackgroundColor(this.resources.getColor(R.color.redColor))
            binding.homeLayout.setBackgroundColor(this.resources.getColor(R.color.bgColor))
            binding.chatLayout.setBackgroundColor(this.resources.getColor(R.color.bgColor))
            startSpecificActivity(PostActivity::class.java)

        }

        binding.homeLayout.setOnClickListener {
            binding.homeLayout.setBackgroundColor(this.resources.getColor(R.color.redColor))
            binding.postLayout.setBackgroundColor(this.resources.getColor(R.color.bgColor))
            binding.chatLayout.setBackgroundColor(this.resources.getColor(R.color.bgColor))

        }

        binding.chatLayout.setOnClickListener {
            binding.chatLayout.setBackgroundColor(this.resources.getColor(R.color.redColor))
            binding.homeLayout.setBackgroundColor(this.resources.getColor(R.color.bgColor))
            binding.postLayout.setBackgroundColor(this.resources.getColor(R.color.bgColor))
        }
    }

    private fun setListAdapter() {
        categoryDetailAdapter = CategoryDetailAdapter(starList,
            object : CategoryDetailAdapter.CategoryDetailListener {
                override fun onItemClickListener(selectedItem: String) {
                }
            })
        binding.categoryList.adapter = categoryDetailAdapter
    }
}
