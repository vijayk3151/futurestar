package app.star.activity.more

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import app.star.R
import app.star.activity.base.BaseActivity
import app.star.activity.login.LoginActivity
import app.star.activity.register.RegisterActivity
import app.star.binding.setSafeOnClickListener
import app.star.databinding.ActivityMoreBinding

class MoreActivity : BaseActivity() {
    lateinit var binding: ActivityMoreBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_more)
        intiView()
    }

    private fun intiView() {

        clickEvent()

    }

    private fun clickEvent() {
        binding.backImage.setSafeOnClickListener {
            finish()
        }

        binding.txtRegister.setSafeOnClickListener {

            startSpecificActivity(RegisterActivity::class.java)
        }

        binding.txtLogin.setSafeOnClickListener {

            startSpecificActivity(LoginActivity::class.java)
        }

        binding.txtBlog.setSafeOnClickListener {

        }
        binding.txtContactUs.setSafeOnClickListener {

        }

        binding.txtPrivacy.setSafeOnClickListener {

        }
        binding.txtTerms.setSafeOnClickListener {


        }

        binding.txtAboutUs.setSafeOnClickListener {

        }
        binding.txtRefundPolicy.setSafeOnClickListener {

        }

    }
}
