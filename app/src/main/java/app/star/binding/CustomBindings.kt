package app.star.binding
import android.view.View
import android.widget.EditText
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import org.jetbrains.annotations.NotNull


@BindingAdapter("error")
        fun EditText.setError(errorMessage: String?) {
            if (errorMessage!="") {
                error = errorMessage
            }
            else{
                error=""
            }
        }


        @BindingAdapter("visibility")
        fun View.setVisibility(value:MutableLiveData<Boolean>){
            setVisibleOrGone(value.value)
        }




