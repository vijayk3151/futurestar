@file:JvmName("ViewExt")
@file:Suppress("unused")
package app.star.binding


import android.view.View
import app.star.util.SafeClickListener
import org.jetbrains.annotations.NotNull

@NotNull
fun View.setVisibleOrGone(visibility: Boolean?) {
    val visible = if (visibility == true) View.VISIBLE else View.GONE
    this.visibility = visible
}


fun View.setVisibleOrInvisible(visibility: Boolean?) {
    val visible = if (visibility == true) View.VISIBLE else View.INVISIBLE
    this.visibility = visible
}

fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
    val safeClickListener = SafeClickListener {
        onSafeClick(it)
    }
    setOnClickListener(safeClickListener)
}
