package app.star.fragment.search

import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import app.star.R
import app.star.activity.detail.CategoryInfo
import app.star.activity.home.HomeActivity
import app.star.adapter.CategoryAdapter
import app.star.adapter.StarSearchAdapter
import app.star.databinding.StarSearchFragmentBinding

class StarSearchFragment : Fragment(){
    lateinit var binding: StarSearchFragmentBinding
    lateinit var searchArray: ArrayList<String>
    lateinit var searchSearchAdapter: StarSearchAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.star_search_fragment, container, false)
        intiView()
        return binding.root
    }

    private fun intiView() {
        makeSpannableText()
        searchArray= ArrayList()
        searchArray.add("cricket")
        searchArray.add("cricket")
        searchArray.add("cricket")
        searchArray.add("cricket")
        setListAdapter()
    }

    private fun setListAdapter() {
        searchSearchAdapter = StarSearchAdapter(searchArray,
            object : StarSearchAdapter.StarSearchAdapterListener {
                override fun onItemClickListener(selectedItem: String) {
                    (activity as HomeActivity).startSpecificActivity(CategoryInfo::class.java)
                }
            })
        binding.searchList.adapter = searchSearchAdapter
    }

    private fun makeSpannableText() {
        val firstPointString  = SpannableString(getString(R.string.search_welcome_text))
        val secondPointString  = SpannableString(getString(R.string.search_welcome_text))
        secondPointString.setSpan(object : ClickableSpan() {
            override fun onClick(v: View) {
            }
            override fun updateDrawState(ds: TextPaint) {
                ds.color = this@StarSearchFragment.requireContext().resources.getColor(R.color.redColor)
            }
        }, secondPointString.length-17, secondPointString.length-11, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        binding.txtWelcome.text= secondPointString
        binding.txtWelcome.setMovementMethod(LinkMovementMethod.getInstance())
        firstPointString.setSpan(object : ClickableSpan() {
            override fun onClick(v: View) {

            }
            override fun updateDrawState(ds: TextPaint) {
                ds.color = this@StarSearchFragment.requireContext().resources.getColor(R.color.yellowColor)

            }
        }, 15, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        binding.txtWelcome.text= firstPointString
        binding.txtWelcome.setMovementMethod(LinkMovementMethod.getInstance())


    }

}