package app.star.fragment.trending

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import app.star.R
import app.star.activity.detail.CategoryInfo
import app.star.activity.home.HomeActivity
import app.star.adapter.TrendingAdapter
import app.star.databinding.TredingFragmentBinding

class TrendingFragment :Fragment(){
    lateinit var binding: TredingFragmentBinding
    lateinit var categoryList:ArrayList<String>
    lateinit var trendingAdapter: TrendingAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.treding_fragment, container, false)
        intiView()
        return binding.root
    }

    private fun intiView() {
        categoryList= ArrayList()
        categoryList.add("cricket")
        categoryList.add("cricket")
        categoryList.add("cricket")
        categoryList.add("cricket")
        setListAdapter()
    }
    private fun setListAdapter() {
        trendingAdapter = TrendingAdapter(categoryList,
            object : TrendingAdapter.TrendingAdapterListener {
                override fun onItemClickListener(selectedItem: String) {
                    (activity as HomeActivity).startSpecificActivity(CategoryInfo::class.java)
                }
            })
        binding.trendingList.layoutManager = GridLayoutManager(this.requireContext(),2)
        binding.trendingList.adapter = trendingAdapter
    }
}