package app.star.fragment.mall

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import app.star.R
import app.star.activity.detail.CategoryInfo
import app.star.activity.home.HomeActivity
import app.star.adapter.CategoryAdapter
import app.star.databinding.MallFragmentBinding

class TalentMallFragment : Fragment(){
    lateinit var binding: MallFragmentBinding
    lateinit var categoryList: ArrayList<String>
    lateinit var categoryAdapter:CategoryAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.mall_fragment, container, false)
        intiView()
        return binding.root
    }

    private fun intiView() {
        categoryList= ArrayList()
        categoryList.add("cricket")
        categoryList.add("cricket")
        categoryList.add("cricket")
        categoryList.add("cricket")
        setListAdapter()
    }

    private fun setListAdapter() {
        categoryAdapter = CategoryAdapter(categoryList,
            object : CategoryAdapter.CategoryAdapterListener {
                override fun onItemClickListener(selectedItem: String) {
                    (activity as HomeActivity).startSpecificActivity(CategoryInfo::class.java)
                }
            })
        binding.categoryList.layoutManager = LinearLayoutManager(this.requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.categoryList.adapter = categoryAdapter
    }

}