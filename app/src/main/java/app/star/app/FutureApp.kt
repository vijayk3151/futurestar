package app.star.app

import android.app.Application
import app.star.module.viewModelModule
import app.star.module.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class FutureApp :Application() {
    override fun onCreate() {
        super.onCreate()
        val modules = listOf(networkModule, viewModelModule)

        startKoin{
            androidLogger()
            androidContext(this@FutureApp)
            modules(modules)
        }

    }

}